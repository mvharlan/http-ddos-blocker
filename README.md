# HTTP DDOS Blocker

This perl script called **ddb** is a simple but effective HTTP/S flooding  DDOS blocker based on geo locations.

If you happen to suffer an HTTP/S flooding DDOS attack on a mostly regional web site, you can very quickly get rid of all the nasty bots attacking your site.

## How does it work

**ddb** uses **tcpdump** to detect all connection attempts to port 80 and 443. It uses **geoiplookup** to find out from which country the request is coming. You can either use a positive list of _good_ countries or a negative list of _bad_ countries and create a blocklist of networks (IP/24), that will be added to a nftables ipset called **nets**. This ipset is linked to the nftables **drop** zone and drops packets from these networks from now on.

So if your site is mostly used by local visitors, you could use a positive list with your own and some other country codes e.g:

    ddb -d -G DE,UK

This will add networks from all other countries to the block list. The list is dumped to _/etc/ddb.nets_ every 30 seconds, so you won't loose your collected networks. **ddb** will read the dumped list on startup and will add more networks as it detects more bots,

If you would like to block some notoriously  bot spoiled countries use e.g.:

    ddb -d -B CN,RU,IN

## Caution

This tool does not work very well, if you are running an international site, that is being used from all over the world, though you might still block connections from a few extraordinary annoying locations. 

## Environment

If you are running a reverse proxy serving several domains, you will find out, that usualle only one of the web sites is being attacked. Check your logfiles, which one it is. If this is your setup, it would be best to redirect this site to another idling machine, by changing your DNS record for this webserver. This will take load from your reverse proxy and your other sites are no longer harmed.

 The affected domain can now be handled on the machine the traffic is redirected to. If you are only running a single domain, redirection does not make sense.

 ## Requirements

 To run the DDOS blocker you will need:

- linux host
- tcpdump
- perl
- geoiplookup
- firewalld and firewall-cmd
- nftables (not tested for iptables)

This all is usually available and pre installed on all Redhat based systems. You can also install it on all Debian based systems.

## Preparations

- Make sure firewalld is running und active

	firewall-cmd --state

- Make sure your firewalld config has a **drop** zone

	firewall-cmd --permanent --get-zones

- Make sure your **drop** zone uses as target either **DROP** or **REJECT**

	firewall-cmd --permanent --info-zone=drop

- Make sure your firewalld has a type 'hash:net' ipset called **nets**

	firewall-cmd --permanent --get-ipsets
	firewall-cmd --permanent --info-ipset=nets

- Make sure the **nets** ipset is used as source for the **drop** zone

	firewall-cmd --permanent --zone=drop --add-source=ipset:nets

## Setup for ddb

Before running the blocker you need to think about a few settings:

### Whitelist of addresses

To make sure your well known IPs are not being blocked, put them into _/etc/ddb.wl_ . One Address per line e.g.:

	cat /etc/ddb.wl

	192.168.1.1
	1.2.3.4
	244.233.222.221

### List of good countries

If you are using a positive list of _good_ countries, you can put them into _/etc/ddb.good_ and call it like:

    ddb -G /etc/ddb.good

If you are using a negative list of _bad_ countries, you can put them into _/etc/ddb.bad_ and call ddb like:

    ddb -B /etc/ddb.bad

The lists should contain the country codes, one per line. You can specify the CC's also on the command line instead of using the file name as argument

    ddb -G US,UK,DE

The files should look like:

	cat /etc/ddb.good
	
	US
	CN
	MX
	..
	..
	
## Running it

**ddb** uses some options, but need in any case either -G GOOD or -B BAD, to known how to make decisions.

Options are:

- `-d        ` for debugging
- `-h        ` for help
- `-G GOOD   ` to specify a good-country file GOOD or a list of good country codes like IN,SP,AR
- `-B BAD    ` to specify a bad-country file BAD or a list of bad country codes like US,UK,SE
- `-A ADDR   ` to specify an ip address whitelist file ADDR or a list of ip addresses like 1.2.3.4,5.6.7.8,... 
- `-I IPSET  ` the name of the ipset to use as blocklist if not using **nets**
- `-C MAXCON ` Allow MAXCON connections in MAXINT even for good networks, to find bots in your good countries
- `-I MAXINT ` Check for MAXCON connections int MAXINT secondes. Block the net even if it's a good country if MAXCON is reached.

You can always stop the programm and restart it again, since the blocklist is stored every 30 seconds and reread at startup. I'ts probably a good idea to stop the webserver or traffic forwarder at first, because it would consume a lot of system resources. Let the blocklist fill first, and start it later again.

A good way to start for somebody in the benelux countries would be:

    ddb -d -G NL,BE,LU

## Recovering your Webserver Availability

If you are running **ddb** on the machine, that hosts the webserver, you can start your webserver after some time, if **ddb** has collected some bot nets and added them to the block list.

If you have redirected your traffic, you could start xinetd to forward your traffic to the real web server, that is hidden by the changed DNS record. You would need a config file like:


	service http
 	{
		disable		= no
		id		    = httpdos
		type		= UNLISTED
		port		= 80
		cps		    = 100 10
		socket_type	= stream
		protocol	= tcp
		log_type	= FILE /var/log/xinetd.log
		log_on_success  += HOST TRAFFIC
		redirect	= your.web.server 80 
		user		= nobody
		wait		= no
	}

	service https
	{
		disable		= no
		id		    = httpsdos
		type		= UNLISTED
		port		= 443
		cps		    = 100 10
		socket_type	= stream
		protocol	= tcp
		log_type	= FILE /var/log/xinetd.log
		log_on_success  += HOST TRAFFIC
		redirect	= your.web.server 443
		user		= nobody
		wait		= no
	}

## Author

Magnus Harlander

## License

**In a few words:**

Feel free to use it as you like, but don't blame me, if it doesn't work for you. No liability on my side for any damage or other  problems.

**In detail (BSD):**

Copyright (c) 2020 by Magnus Harlander. All rights reserved. Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
- Neither the name of the nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

